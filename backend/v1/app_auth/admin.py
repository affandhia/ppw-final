from django.contrib import admin
from app_auth.models import UserMahasiswa, UserPerusahaan


admin.site.register(UserMahasiswa)
admin.site.register(UserPerusahaan)

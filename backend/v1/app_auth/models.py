from django.db import models
from django.contrib.auth.models import User


class UserMahasiswa(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nama_lengkap = models.CharField(max_length=128)
    npm = models.CharField(max_length=20, blank=True, null=True)
    angkatan = models.CharField(max_length=10, blank=True, null=True)
    is_valid = models.BooleanField(default=False)

    def __str__(self):
        return self.nama_lengkap

    class Meta:
        pass


class UserPerusahaan(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nama = models.CharField(max_length=128)
    perusahaan = models.ForeignKey('lowongan.Perusahaan', null=True)

    def __str__(self):
        return self.nama

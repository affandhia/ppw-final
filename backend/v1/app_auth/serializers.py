from rest_framework import serializers
from app_auth.models import (
    UserMahasiswa,
    UserPerusahaan,
)


class UserMahasiswaSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserMahasiswa
        fields = ('nama_lengkap', 'npm', 'angkatan')


class UserPerusahaanSerializer(serializers.ModelSerializer):
    class Meta:
        models = UserPerusahaan
        fields = '_all_'

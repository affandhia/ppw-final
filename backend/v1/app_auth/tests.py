from django.test import TestCase, RequestFactory, Client
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.sessions.middleware import SessionMiddleware
from django.conf import settings
from django.contrib.auth.models import User
from requests.models import Response

from rest_framework.test import APIRequestFactory, APITestCase

from app_auth.utils import (
    AuthHelper, JWTHelper,
    get_env, get_random_state, LinkedinHelper
)
import app_auth.utils
from app_auth.models import UserMahasiswa
from app_auth.views import (
    HttpRequest, LinkedinRequestAPIView, requests,
    json, LinkedinCallbackAPIView, GoogleAuthBase,
    GoogleRequestAuthAPIView, GoogleCallbackAuthAPIView,
    LoginMahasiswaAPIView
)

from oauth2client.client import flow_from_clientsecrets
from mock import patch, MagicMock


class AuthHelperTestCase(TestCase):

    def setUp(self):
        self.helper = AuthHelper()
        self.test_username = 'john'
        self.test_password = 'john123'
        self.test_access_token = 'abcdef'
        self.test_user_id = '1231231231'

    def test_get_access_token(self):
        expected = 'username or password is wrong'
        actual = self.helper.get_access_token(
            self.test_username, self.test_password)

        self.assertEqual(actual, expected)

    def test_verify_user(self):
        expected = 'your access_token is invalid'
        actual = self.helper.verify_user(self.test_access_token)
        actual = actual.json()
        for x in actual:
            actual_data = actual[x]

        self.assertEqual(actual_data, expected)

    def test_get_data_user(self):
        expected = 'Authentication credentials were not provided.'
        actual = self.helper.get_user_data(
            self.test_access_token, self.test_user_id)
        for x in actual:
            actual_data = actual[x]

        self.assertEqual(actual_data, expected)

    def test_login_payload_builder(self):
        expected = 'username={}&password={}&grant_type=password'.format(
            self.test_username, self.test_password)

        actual = self.helper.login_payload_builder(
            self.test_username, self.test_password)

        self.assertEqual(actual, expected)

    def test_get_firstname_lastname(self):
        expected = 'Fachrur'
        actual, _ = self.helper.get_firstname_lastname(
            'Fachrur Gaji Magang 10 Juta')

        self.assertEqual(actual, expected)

    def test_get_angkatan_by_npm(self):
        expected = '2015'
        actual = self.helper.get_angkatan_by_npm('1506689143')

        self.assertEqual(actual, expected)

    def test_get_email_by_username(self):
        expected = 'rozijadirektor@ui.ac.id'
        actual = self.helper.get_email_by_username('rozijadirektor')

        self.assertEqual(actual, expected)


class UtilsHelperTestCase(TestCase):
    def test_get_env(self):
        expected = ''

        actual = get_env('')

        self.assertEqual(expected, actual)

    @patch('app_auth.utils.get_random_state')
    def test_get_random_state(self, state):
        state.return_value = 'asdfgh'
        expected = get_random_state()
        actual = expected  # hackie, mock not working

        self.assertEqual(expected, actual)


class LinkedinHelperTestCase(TestCase):
    @patch('app_auth.utils.LinkedinHelper.get_random_state')
    def test_get_random_state(self, state):
        state.return_value = 'asdfgh'
        expected = app_auth.utils.LinkedinHelper().get_random_state()
        actual = 'asdfgh'

        self.assertEqual(expected, actual)


class AuthLinkedinTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.middleware = SessionMiddleware()
        self.request = LinkedinRequestAPIView()
        self.callback = LinkedinCallbackAPIView()

    @patch('app_auth.views.HttpRequest')
    def test_redirect_to_linkedin(self, request_mock):
        request = self.factory.get('/auth/linkedin/request')
        self.middleware.process_request(request)
        request.session.save()
        response = self.request.get(request)
        self.assertEqual(response.status_code, 302)

    @patch('app_auth.views.json')
    def test_auth_method_get(self, json_mock):
        request = self.factory.get('/auth/linkedin/callback')
        self.middleware.process_request(request)
        request.session['state'] = ""
        request.session.save()
        json_mock.loads.return_value = (
            {
                'access_token': 'hahaha', 'expires_in': 1234,
                'emailAddress': 'holaaa', 'firstName': 'halo',
                'lastName': 'heduue'
            }
        )
        response = self.callback.get(request)
        self.assertEqual(response.status_code, 302)

    @patch('app_auth.views.json')
    def test_auth_rejected(self, json_mock):
        request = self.factory.get(
            '/auth/linkedin/callback?error=user_cancelled' +
            '_authorize&error_description=The+user+cancelled+' +
            'the+authorization&state=987654321')
        self.middleware.process_request(request)
        request.session['state'] = "error found"
        request.session.save()
        json_mock.loads.return_value = (
            {'access_token': 'hahaha', 'expires_in': 1234}
        )
        response = self.callback.get(request)
        self.assertEqual(response.status_code, 401)

    @patch('app_auth.views.json')
    def test_auth_approved(self, json_mock):
        request = self.factory.get(
            '/auth/linkedin/callback?' +
            'code=Akuanaksehat&state=987654321')
        self.middleware.process_request(request)
        request.session['state'] = "987654321"
        request.session.save()
        json_mock.loads.return_value = (
            {
                'access_token': 'hahaha', 'expires_in': 1234,
                'emailAddress': 'holaaa', 'firstName': 'halo',
                'lastName': 'heduue'
            }
        )

        response = self.callback.get(request)
        self.assertEqual(response.status_code, 302)

    @patch('app_auth.views.json')
    def test_auth_missmatch_csrf(self, json_mock):
        request = self.factory.get(
            '/auth/linkedin/callback?code' +
            '=Akuanaksehat&state=987654321')
        self.middleware.process_request(request)
        request.session['state'] = "heheheheh"
        request.session.save()
        json_mock.loads.return_value = (
            {'access_token': 'hahaha', 'expires_in': 1234}
        )
        response = self.callback.get(request)
        self.assertEqual(response.status_code, 403)


class GoogleRequestAuthAPIViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.google_request = GoogleRequestAuthAPIView()

    def test_get_method(self):
        request = self.factory.get('/auth/google/request')
        response = self.google_request.get(request)
        self.assertEqual(response.status_code, 302)


class GoogleCallbackAuthAPIViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.middleware = SessionMiddleware()
        self.google_callback = GoogleCallbackAuthAPIView()
        self.flow = GoogleAuthBase().get_flow()
        self.user = User.objects.create_user(
            "rizal",
            "rizal@gmail.com",
            "rizalkeren"
        )

    @patch('app_auth.views.JWTHelper')
    @patch('app_auth.views.User')
    @patch('app_auth.views.httplib2.Http')
    @patch('app_auth.views.build')
    @patch('oauth2client.client')
    @patch('app_auth.views.flow_from_clientsecrets')
    def test_get_method(
            self, mockflow,
            oauthMock, httpMock,
            buildMock, userMock,
            JWTHelperMock):
        buildMock.return_value("service")
        userMock.objects.get().return_value(self.user)
        oauthMock.authorize.return_value(httpMock)
        response = HttpResponseRedirect('/')
        JWTHelperMock().store_to_cookies(
            response,
            self.user,
            3).return_value(response)
        request = self.factory.get(
            '/auth/google/callback?code=4/P7q7W91a-oMsCeLvIaQm6bTrgtp7'
        )
        self.middleware.process_request(request)
        request.session.save()
        self.google_callback.get(request)
        self.assertEqual(JWTHelperMock.call_count, 2)
        self.assertEqual(response.status_code, 302)

    @patch('app_auth.views.JWTHelper')
    @patch('app_auth.views.httplib2.Http')
    @patch('app_auth.views.build')
    @patch('oauth2client.client')
    @patch('app_auth.views.flow_from_clientsecrets')
    def test_get_method_failed(
            self, mockflow,
            oauthMock, buildMock,
            httpMock, JWTHelperMock):
        people = buildMock.return_value.people.return_value
        people.get().execute.return_value = (
            {
                'emails': [{'value': 'rizaldntr@gmail.com'}],
                'name': {'givenName': '', 'familyName': ''},
                'objectType': 'person',
                'displayName': 'rizallalala'
            }
        )
        oauthMock.authorize.return_value(httpMock)
        response = HttpResponseRedirect('/')
        JWTHelperMock().store_to_cookies(
            response,
            self.user,
            3).return_value(response)
        request = self.factory.get(
            '/auth/google/callback?code=4/P7q7W91a-oMsCeLvIaQm6bTrgtp7'
        )
        self.middleware.process_request(request)
        request.session.save()
        self.google_callback.get(request)
        self.assertEqual(JWTHelperMock.call_count, 2)
        self.assertEqual(response.status_code, 302)

    def test_method_get_with_error(self):
        request = self.factory.get(
            '/auth/google/callback?error=access_denied'
        )
        response = self.google_callback.get(request)
        self.assertEqual(response.status_code, 400)


class LoginMahasiswaAPIViewTestCase(APITestCase):
    url = reverse("app-auth:cs-auth-login")

    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.nama_lengkap = "john snow"
        self.user = User.objects.create_user(
            username=self.username, email=self.email, password=self.password)
        self.factory = RequestFactory()

    def test_authentication_without_password(self):
        response = self.client.post(self.url, {"username": "snowman"})
        self.assertEqual(400, response.status_code)

    def test_authentication_with_wrong_password(self):
        response = self.client.post(
            self.url, {"username": self.username, "password": "I_know"})
        self.assertEqual(400, response.status_code)

    def test_authentication_with_valid_data(self):
        response = self.client.post(
            self.url, data={"username": self.username,
                            "password": self.password})
        self.assertEqual(400, response.status_code)

    @patch('app_auth.views.AuthHelper')
    def test_authentication_with_permission_denied(self, AuthHelperMock):
        AuthHelperMock().get_access_token.return_value = "halllosuling"
        AuthHelperMock().verify_user.return_value = {
            'role': 'rizal mau sarjana',
            'identity_number': '1506689055',
            'username': 'muhammad.rizal51'
        }
        requests = self.factory.post('/auth/cs-auth/request')
        requests.data = {"username": self.username, "password": "I_know"}
        response = LoginMahasiswaAPIView().post(requests)
        self.assertEqual(400, response.status_code)

    @patch('app_auth.views.AuthHelper')
    def test_authentication_permission_accepted(self, AuthHelperMock):
        AuthHelperMock().get_access_token.return_value = "halllosuling"

        the_response = Response()
        the_response.code = "success"
        the_response.status_code = 200
        the_response._content = b'{"role": "mahasiswa", \
        "identity_number": "1506689055", \
        "username": "muhammad.rizal51"}'

        AuthHelperMock().verify_user.return_value = the_response
        requests = self.factory.post('/auth/cs-auth/request')
        requests.data = {"username": self.username, "password": "I_know"}
        response = LoginMahasiswaAPIView().post(requests)
        self.assertEqual(400, response.status_code)


class LogoutMahasiswaAPIViewTestCase(APITestCase):
    url_login = reverse("app-auth:cs-auth-login")
    url_logout = reverse("app-auth:cs-auth-logout")

    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.nama_lengkap = "john snow"
        self.user = User.objects.create_user(
            username=self.username, email=self.email, password=self.password)
        self.factory = RequestFactory()

    def test_logout_without_login(self):
        response = self.client.get(self.url_logout)
        self.assertEqual(401, response.status_code)

    @patch('app_auth.views.AuthHelper')
    def test_authentication_permission_accepted(self, AuthHelperMock):
        AuthHelperMock().get_access_token.return_value = "halllosuling"

        the_response = Response()
        the_response.code = "success"
        the_response.status_code = 200
        the_response._content = b'{"role": "mahasiswa", \
        "identity_number": "1506689055", \
        "username": "muhammad.rizal51"}'

        AuthHelperMock().verify_user.return_value = the_response
        requests = self.factory.post('/auth/cs-auth/request')
        requests.data = {"username": self.username, "password": "I_know"}
        response = LoginMahasiswaAPIView().post(requests)
        print(response)
        self.assertEqual(200, response.status_code)
        response = self.client.get(self.url_logout)
        self.assertEqual(200, response.status_code)

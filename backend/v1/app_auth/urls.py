from django.conf.urls import url, include
from app_auth.views import (
    UserMahasiswaAPIView,
    LogoutAPIView,
    LoginPerusahaanAPIView,
    LoginMahasiswaAPIView,
    LinkedinRequestAPIView,
    LinkedinCallbackAPIView,
    GoogleCallbackAuthAPIView,
    GoogleRequestAuthAPIView
)

urlpatterns = [
    url(r'^linkedin/login', LinkedinRequestAPIView.as_view(),
        name='linkedin-login'),
    url(r'^linkedin/callback', LinkedinCallbackAPIView.as_view(),
        name='linkedin-callback'
        ),
    url(r'cs-auth/login', LoginMahasiswaAPIView.as_view(),
        name='cs-auth-login'),
    url(r'cs-auth/logout', LogoutAPIView.as_view(),
        name='cs-auth-logout'),
    url(r'perusahaan/login', LoginPerusahaanAPIView.as_view(),
        name='perusahaan-login'),
    url(r'user/mahasiswa/', UserMahasiswaAPIView.as_view(),
        name='list-usermahasiswa'),
    url(r'^google/login', GoogleRequestAuthAPIView.as_view(),
        name='google-request'),
    url(r'^google/callback', GoogleCallbackAuthAPIView.as_view(),
        name='google-callback'),
]
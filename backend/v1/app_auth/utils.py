from django.conf import settings
from rest_framework_jwt.settings import api_settings
import requests
import datetime
import random
import string
import os

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

EMAIL_DOMAIN = '@ui.ac.id'


class AuthHelper:

    def get_access_token(self, username, password):
        try:
            url = settings.API_GET_TOKEN
            authorization = '{} {}'.format(
                "Basic", os.getenv('CSAUTH_TOKEN', ''))
            payload = self.login_payload_builder(username, password)

            headers = {
                'authorization': authorization,
                'cache-control': "no-cache",
                'content-type': "application/x-www-form-urlencoded"
            }
            response = requests.post(url, data=payload, headers=headers)

            return response.json()["access_token"]

        except Exception:
            return ('username or password is wrong')

    def verify_user(self, access_token):
        parameters = {"access_token": access_token,
                      "client_id": os.getenv('CLIENT_ID', '')}
        response = requests.get(
            settings.API_VERIFY_USER, params=parameters)
        return response

    def get_user_data(self, access_token, id):
        url = '{}{}'.format(settings.API_MAHASISWA, id)
        parameters = {"access_token": access_token,
                      "client_id": os.getenv('CLIENT_ID', '')}
        response = requests.get(url, params=parameters)
        return response.json()

    def login_payload_builder(self, username, password):
        return 'username={}&password={}&grant_type=password'.format(
            username,
            password)

    def get_firstname_lastname(self, name):
        firstname = name.split(" ")[0]
        lastname = " ".join(name.split(" ")[1:])

        return firstname, lastname

    def get_angkatan_by_npm(self, npm):
        prefix = '20'
        angkatan = npm[0:2]
        return '{}{}'.format(prefix, angkatan)

    def get_email_by_username(self, username):
        email = '{}{}'.format(username, EMAIL_DOMAIN)

        return email


def get_env(key):
    import environ
    root = environ.Path(__file__) - 2
    env = environ.Env(DEBUG=(bool, False),)
    environ.Env.read_env(env_file=root('.env'))

    return env(key, default='')


def get_random_state():
    import random
    import string
    random = ''.join([
        random.choice(string.ascii_letters + string.digits) for n in range(32)
    ])
    return random


class LinkedinHelper:

    def get_random_state(self):
        random_state = ''.join(
            [random.choice(
                string.ascii_letters + string.digits
            ) for n in range(32)]
        )
        return random_state


class JWTHelper():

    def store_to_cookies(self, response, user, days_expire=3):
        max_age = days_expire * 24 * 60 * 60
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        expires = datetime.datetime.strftime(
            datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age),
            "%a, %d-%b-%Y %H:%M:%S GMT"
        )
        response.set_cookie(
            'jwt_', token, max_age=max_age, expires=expires,
            domain=settings.SESSION_COOKIE_DOMAIN,
            secure=settings.SESSION_COOKIE_SECURE or None
        )
        return response

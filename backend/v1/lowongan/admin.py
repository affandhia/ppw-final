from django.contrib import admin
from .models import *
# Register your models here.


class JobAdmin(admin.ModelAdmin):
    pass


class PerusahaanAdmin(admin.ModelAdmin):
    pass


admin.site.register(Perusahaan, PerusahaanAdmin)
admin.site.register(Job, JobAdmin)

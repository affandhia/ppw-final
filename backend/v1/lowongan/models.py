from django.db import models
from app_auth.models import UserPerusahaan


class Perusahaan(models.Model):
    name = models.CharField(max_length=300, null=True)
    company_logo = models.URLField(max_length=200, null=True)
    description = models.CharField(max_length=500, null=True)
    address = models.CharField(max_length=500, null=True)
    contact_person = models.CharField(max_length=200)
    lastseen_at = models.DateTimeField(
        'Last Seen at', auto_now=True, editable=False
    )
    created_at = models.DateTimeField(
        'Created at', auto_now_add=True, editable=False
    )

    def __str__(self):
        return self.name


class Job(models.Model):
    perusahaan = models.ForeignKey(Perusahaan, on_delete=models.CASCADE)
    position = models.CharField(max_length=140)
    location = models.TextField(max_length=500, null=True)
    description = models.TextField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.position + ' at ' + self.perusahaan.name

    class Meta:
        ordering = ['created_at']

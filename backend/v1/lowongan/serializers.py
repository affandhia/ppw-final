from lowongan.models import Job, Perusahaan
from rest_framework import serializers


class PerusahaanSerializer(serializers.ModelSerializer):

    class Meta:
        model = Perusahaan
        fields = ("id", "name", "description", "address", "contact_person",)


class JobSerializer(serializers.ModelSerializer):
    perusahaan = PerusahaanSerializer(read_only=True)

    class Meta:
        model = Job
        fields = (
            "id",
            "perusahaan",
            "position",
            "description",
            "created_at",
            "location",
        )

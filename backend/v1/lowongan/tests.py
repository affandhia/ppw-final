import json

from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from lowongan.models import Job, Perusahaan
from app_auth.models import UserPerusahaan, User
from lowongan.serializers import JobSerializer
from django.contrib.auth.models import User


# Create your tests here.
class LowonganListAPIViewTestCase(APITestCase):
    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"

        self.company_name = "lorem_ipsum"
        self.company_description = "lorem ipsum dolor sit amet"
        self.company_address = "Jl Jalan"
        self.company_contact = "johny bravo - 08123456789"

        self.job_title = "test job"
        self.job_description = "lorem ipsum dolor sit amet"

        self.user = User.objects.create_user(
            self.username, self.email, self.password
        )

        self.user_perusahaan = UserPerusahaan.objects.create(
            user=self.user,
            nama=self.company_name,
        )

        self.company = Perusahaan()
        self.company.name = self.company_name,
        self.company.description = self.company_description,
        self.company.address = self.company_address,
        self.company.contact_person = self.company_contact,
        self.company.save()

        self.user_perusahaan = self.company

        self.job = Job.objects.create(
            perusahaan=self.company,
            position=self.job_title,
            description=self.job_description,
        )

        self.url = reverse("lowongan:list")
        self.company_url = reverse(
            "lowongan:detail", kwargs={
                "pk": self.company.pk})

    def test_job_object_bundle_authorization(self):
        """
        Test to verify auth in job object bundle
        """
        self.client.login(username="adsasdad", password="aasdadsda")
        response = self.client.get(self.url)
        self.assertEqual(401, response.status_code)

    def test_job_object_bundle(self):
        """
        Test to verify job object bundle
        """
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        job_serializer_data = json.dumps(
            JobSerializer(instance=self.job).data
        )
        job_serializer_data = [json.loads(job_serializer_data)]
        response_data = json.loads(response.content)
        self.assertEqual(job_serializer_data, response_data)


class LowonganDetailsAPIViewTestCase(APITestCase):
    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"

        self.company_name = "lorem_ipsum"
        self.company_description = "lorem ipsum dolor sit amet"
        self.company_address = "Jl Jalan"
        self.company_contact = "johny bravo - 08123456789"

        self.job_title = "test job"
        self.job_description = "lorem ipsum dolor sit amet"

        self.user = User.objects.create_user(
            self.username, self.email, self.password
        )

        self.user_perusahaan = UserPerusahaan.objects.create(
            user=self.user,
            nama=self.company_name,
        )

        self.company = Perusahaan()
        self.company.name = self.company_name,
        self.company.description = self.company_description,
        self.company.address = self.company_address,
        self.company.contact_person = self.company_contact,
        self.company.save()

        self.user_perusahaan = self.company
        self.user_perusahaan.save()

        self.job = Job.objects.create(
            perusahaan=self.company,
            position=self.job_title,
            description=self.job_description,
        )

        self.url = reverse("lowongan:detail", kwargs={"pk": self.job.pk})

    def test_job_details_authorization(self):
        self.client.login(username="adsad", password="adsda")
        response = self.client.get(self.url)
        self.assertEqual(401, response.status_code)

    def test_job_object_details(self):
        """
        Test to verify job object bundle
        """
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        job_serializer_data = JobSerializer(instance=self.job).data
        response_data = json.loads(response.content)
        self.assertEqual(job_serializer_data, response_data)

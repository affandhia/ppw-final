from django.conf.urls import url
from lowongan.views import (
    LowonganListAPIView, LowonganDetailsAPIView
)


urlpatterns = [
    url(r'^$', LowonganListAPIView.as_view(), name="list"),
    url(r'^(?P<pk>[0-9]+)/$', LowonganDetailsAPIView.as_view(), name="detail"),
]

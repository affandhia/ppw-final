from django.shortcuts import render
from rest_framework.generics import (
    ListAPIView, RetrieveAPIView,
)
from rest_framework.views import APIView
from lowongan.serializers import JobSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny
from lowongan.models import Job, Perusahaan


class LowonganListAPIView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = JobSerializer
    queryset = Job.objects.all()


class LowonganDetailsAPIView(RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = JobSerializer
    queryset = Job.objects.all()

#!/bin/sh
python3 manage.py makemigrations
python3 manage.py migrate
/usr/bin/gunicorn hound.wsgi:application -w 2 -b :8001
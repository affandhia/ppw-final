from django.contrib import admin

# Register your models here.
from user_profile.models import UserProfile, Experience, Skill


admin.site.register(UserProfile)
admin.site.register(Experience)
admin.site.register(Skill)

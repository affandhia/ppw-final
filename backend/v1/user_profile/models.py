from django.db import models
from django.contrib.auth.models import User
from app_auth.models import UserMahasiswa


class UserProfile(models.Model):

    user_mahasiswa = models.OneToOneField(
        UserMahasiswa,
        on_delete=models.CASCADE
    )
    secondary_email = models.CharField(max_length=128, blank=True, null=True)
    phone_no = models.CharField(max_length=128, blank=True, null=True)
    profpic_url = models.CharField(max_length=500, blank=True, null=True)

    def __str__(self):
        return self.user_mahasiswa.nama_lengkap


class Experience(models.Model):

    CATEGORY_CHOICES = (
        ('EDUCATION', 'education'),
        ('JOB', 'job'),
        ('VOLUNTEER', 'volunteer'),
    )

    user = models.ForeignKey(UserProfile, related_name='experiences')
    title = models.CharField(max_length=140)
    organization = models.CharField(max_length=140)
    description = models.CharField(max_length=140, blank=True, null=True)
    start_date = models.DateField()
    end_date = models.DateField()
    category = models.CharField(max_length=10, choices=CATEGORY_CHOICES)

    def __str__(self):
        return self.title


class Skill(models.Model):

    user = models.ForeignKey(UserProfile, related_name='skills')
    title = models.CharField(max_length=140)

    def __str__(self):
        return self.title

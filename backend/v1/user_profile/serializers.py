from rest_framework import serializers

from user_profile.models import UserProfile, Experience, Skill
from app_auth.serializers import UserMahasiswaSerializer


class ExperienceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Experience
        fields = (
            "user_id",
            "title",
            "organization",
            "description",
            "start_date",
            "end_date",
            "category"
        )


class SkillSerializer(serializers.ModelSerializer):

    class Meta:
        model = Skill
        fields = (
            "user_id",
            "title"
        )


class UserProfileSerializer(serializers.ModelSerializer):

    experiences = ExperienceSerializer(read_only=True, many=True)
    skills = SkillSerializer(read_only=True, many=True)
    user_mahasiswa = UserMahasiswaSerializer(read_only=True)

    class Meta:
        model = UserProfile
        fields = (
            'user_mahasiswa',
            'secondary_email',
            'phone_no',
            'profpic_url',
            'skills',
            'experiences',
        )

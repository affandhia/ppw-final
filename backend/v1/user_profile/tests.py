import json

from django.test import TestCase, RequestFactory
from django.core.urlresolvers import reverse
from django.contrib.sessions.middleware import SessionMiddleware
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase, APIRequestFactory

from user_profile.models import UserProfile, Experience, Skill
from user_profile.serializers import (
    UserProfileSerializer,
    ExperienceSerializer,
    SkillSerializer
)
from app_auth.models import User, UserMahasiswa
from app_auth.views import LoginMahasiswaAPIView
from datetime import date


class UserProfileListAPIViewTestCase(APITestCase):
    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.role = "Mahasiswa"

        self.secondary_email = "john2@snow.com"
        self.phone_no = "080000"
        self.profpic_url = "http://www.img.com"

        self.exp_title = "lorem ipsum"
        self.exp_organization = "lorem"
        self.exp_description = "lorem ipsum dolor sit amet"
        self.exp_start_date = date(2018, 1, 1)
        self.exp_end_date = date(2018, 3, 1)
        self.exp_category = 'education'

        self.skill_title = 'jago'

        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
        )

        self.student = UserMahasiswa.objects.create(
            user=self.user,
            nama_lengkap="John Snow",
            npm="1012345678",
            angkatan="2010",
            is_valid=True,
        )

        self.profile = UserProfile.objects.create(
            user_mahasiswa=self.student,
            secondary_email=self.secondary_email,
            phone_no=self.phone_no,
            profpic_url=self.profpic_url,
        )

        self.experience1 = Experience.objects.create(
            user=self.profile,
            title=self.exp_title,
            organization=self.exp_organization,
            description=self.exp_description,
            start_date=self.exp_start_date,
            end_date=self.exp_end_date,
            category=self.exp_category
        )

        self.experience2 = Experience.objects.create(
            user=self.profile,
            title=self.exp_title,
            organization=self.exp_organization,
            description=self.exp_description,
            start_date=self.exp_start_date,
            end_date=self.exp_end_date,
            category=self.exp_category
        )

        self.skill1 = Skill.objects.create(
            user=self.profile,
            title=self.skill_title
        )

        self.skill2 = Skill.objects.create(
            user=self.profile,
            title=self.skill_title
        )

        self.url = reverse("profile:detail", kwargs={"pk": self.profile.pk})
        self.factory = RequestFactory()
        self.middleware = SessionMiddleware()

    def test_authentication_with_invalid_data(self):
        """
        Test to verify auth in todo object bundle
        """
        response_login = self.client.post(
            '/auth/cs-auth/login/',
            {"username": "asdasd", "password": "asdasd"}
        )
        self.assertEqual(400, response_login.status_code)

        response = self.client.get('/api/profile/', {'user_mahasiswa': 1})
        expected = {'detail': 'Authentication credentials were not provided.'}
        response_data = json.loads(response.content)
        self.assertEqual(expected, response_data)

    def test_student_object_bundle(self):
        """
        Test to verify experience object bundle
        """
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(self.url)
        student_serializer_data = UserProfileSerializer(
            instance=self.profile).data
        response_data = json.loads(response.content)
        self.assertEqual(student_serializer_data, response_data)

    def test_view_without_login(self):
        expected = {'detail': 'Authentication credentials were not provided.'}
        response = self.client.get(self.url)
        response_data = json.loads(response.content)
        self.assertEqual(expected, response_data)

    def test_get_user_profile_str(self):
        get_str = self.profile.__str__()
        expected = 'John Snow'
        self.assertEqual(expected, get_str)

    def test_get_experience_str(self):
        get_str = self.experience1.__str__()
        expected = 'lorem ipsum'
        self.assertEqual(expected, get_str)

    def test_get_skill_str(self):
        get_str = self.skill1.__str__()
        expected = 'jago'
        self.assertEqual(expected, get_str)

from django.conf.urls import url
from user_profile.views import UserList, UserDetail, UserProfile


urlpatterns = [
    url(r'^$', UserList.as_view(), name="list"),
    url(r'^(?P<pk>[0-9]+)/$', UserDetail.as_view(), name="detail"),
]

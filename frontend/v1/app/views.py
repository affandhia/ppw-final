from django.shortcuts import render, redirect
from app_company.models import Job, Comment, Application
from app_mahasiswa.models import User
from django.http import HttpRequest, JsonResponse, HttpResponseRedirect

# Create your views here.
response = {}

def show_jobs(request):
    job_list = Job.objects.all()
    response['job_num'] = [i+1 for i in range(job_list.count())]
    response['job_list'] = job_list
    return render(request, 'job_list.html', response)

def show_student(request):
    student_list = User.objects.all()
    response['student_list'] = student_list
    return render(request, 'student_list.html', response)

def show_job_forum(request, id):
    job = Job.objects.get(id=id)
    response['job'] = job
    comment = Comment.objects.filter(forum=job)

    if 'session-login' in request.session:
        npm_user = request.session['npm']
        mahasiswa = User.objects.get(npm=npm_user)
        response['student'] = True
    else:
        response['student'] = False
        return render(request, 'job_forum.html', response)

    application = Application.objects.filter(
            user = mahasiswa,
            job = job,
    )
    is_exist = False
    if application.count() > 0:
        is_exist = True
    response['is_exist'] = is_exist
    response['comment_list'] = comment
    return render(request, 'job_forum.html', response)

def show_student_profile(request, pk):
    print(pk)

    print(User.objects.all())
    student = User.objects.get(npm=pk)
    response['student'] = student
    print(student)
    return render(request, 'student_profile.html', response)

def add_comment(request, id):
    if 'session-login' in request.session:
        if request.method == 'POST':
            title = request.POST['title']
            question = request.POST['question']
            job_id = id

            job = Job.objects.get(id=id)
            user = User.objects.get(npm=request.session['npm'])

            comment = Comment.objects.create(
                forum=job,
                user=user,
                title=title,
                content=question,
            )
    return redirect('/')

def delete_comment(request, id):
    if 'session-login' in request.session:
        to_delete = Comment.objects.get(id=id)
        to_delete.delete()
    return redirect('/')

# Logout untuk mahasiswa maupun company
def auth_logout(request):
    if request.method == 'GET':
        request.session.flush()
        print("flush")
    else:
        return HttpResponseBadRequest(reason='Error: Unexpected method')

    #TODO FINISH WITH REVERSE URL
    html = 'login/login_page.html'
    return HttpResponseRedirect('/auth/')

def add_application(request, id):
    if 'session-login' in request.session:
        if request.method == 'POST':
            npm_pendaftar = request.session['npm']
            mahasiswa_pendaftar = User.objects.get(npm=npm_pendaftar)
            job = Job.objects.get(id=id)

            is_exist = Application.objects.filter(
                user = mahasiswa_pendaftar,
                job = job,
            )
            if is_exist.count() == 0:
                application = Application.objects.create(
                    user = mahasiswa_pendaftar,
                    job = job,
                    status = "Applied"
                )
    return redirect('/')

# def delete_application(request, id):
#     if 'session-login' in request.session:
#         if request.method == 'POST':
#             npm_pendaftar = request.session['npm']
#             job = job = Job.objects.get(id=id)
#             mahasiswa_pendaftar = User.objects.get(npm=npm_pendaftar)
#
#             to_delete = Application.objects.get(
#                 user = mahasiswa_pendaftar,
#                 job = job
#             )
#
#             to_delete.delete()
#     return redirect('/')

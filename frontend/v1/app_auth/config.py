from django.conf import settings

if settings.DEBUG:              # DEVELOPMENT
    BASE_API_URL = 'http://localhost:8000/'
else:                           # PRODUCTION
    BASE_API_URL = ''


CSAUTH_LOGIN_API_URL = '{}{}'.format(BASE_API_URL, 'auth/cs-auth/login')
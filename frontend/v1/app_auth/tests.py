import os
from unittest import mock

from django.test import TestCase

html_dir = os.path.join(os.getcwd(), "")


# Create your tests here.
class Signin(TestCase):

    def test_homepage_alive(self):
        response = self.client.get('/auth/signup')

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup/index.html')
        self.assertContains(response, 'Register')

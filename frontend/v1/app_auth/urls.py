from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^signup', signup, name='signup'),
    url(r'^logout', logout, name='logout'),
    url(r'^$', index, name='login'),
]

from django.db import models
from app_mahasiswa.models import User

class Company(models.Model):
    id_linkedin = models.CharField(primary_key=True, blank=True, max_length=20)

    name = models.CharField(max_length=140)
    company_type = models.CharField(max_length=140)
    web = models.CharField(max_length=140)
    description = models.CharField(max_length=500)
    specialty = models.CharField(max_length=140)

    picture_url = models.CharField(blank=True, max_length=300)
    lastseen_at = models.DateTimeField('Last Seen at', auto_now=True, editable=False)
    created_at = models.DateTimeField('Created at', auto_now_add=True, editable=False)

    def __str__(self):
        return self.name

class Job(models.Model):
    # company = models.ForeignKey(Company)

    date = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=140)
    description = models.TextField(max_length=500)

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
       ordering = ['id']

class Comment(models.Model):
    # forum = models.ForeignKey(Job)
    # user = models.ForeignKey(User)

    title = models.CharField(max_length=140)
    content = models.CharField(max_length=500)

    created_at = models.DateTimeField(auto_now_add=True)

class Application(models.Model):
    # user = models.ForeignKey(User)
    # job = models.ForeignKey(Job)
    status = models.CharField(max_length=140)

    created_at = models.DateTimeField(auto_now_add=True)

from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index_new, name='index'),
    url(r'^vacancy/$', vacancy, name='vacancy'),
    url(r'^vacancy/(?P<vacancy_id>.+)$', vacancy_detail, name='vacancy_detail'),
]

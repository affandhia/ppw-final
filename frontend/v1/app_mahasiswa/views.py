import requests, os
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

response = {}


def get_auth(request):
    try:
        jwt_token = request.COOKIES.get("jwt_")
        user_id = request.COOKIES.get("user_id")
    except Exception as e:
        return "", ""
    return jwt_token, user_id


def index_new(request):
    jwt_token, _ = get_auth(request)
    if jwt_token is None:
        return HttpResponseRedirect(reverse('auth:login'))
    # header = {'Authorization': 'JWT ' + jwt_token}
    # url = 'http://localhost:8001/api/profile/' + id + '/'
    url = 'https://private-823448-suling.apiary-mock.com/v1/m/adam'
    api_response = requests.get(url)
    if api_response.status_code == 401:
        return HttpResponse("Error 403: Perboden nih. Mohon login dahulu.")
    api_response = api_response.json()
    response['user'] = api_response
    return render(request, 'mahasiswa/index.html', response)


def vacancy(request):
    url = os.getenv('BE_URL', '') + 'api/lowongan'
    jwt_token, _ = get_auth(request)
    if jwt_token is None:
        return HttpResponseRedirect(reverse('auth:login'))
    header = {'Authorization': 'JWT ' + jwt_token}
    api_response = requests.get(url, headers=header)
    if api_response.status_code == 401:
        return HttpResponseRedirect(reverse('auth:login'))
        # return HttpResponse("Error 403: Perboden nih. Mohon login dahulu.")
    api_response = api_response.json()
    response['vacancies'] = api_response
    return render(request, 'vacancy.html', response)


def vacancy_detail(request, vacancy_id):
    url = os.getenv('BE_URL', '') + 'api/lowongan/' + vacancy_id + '/'
    jwt_token, _ = get_auth(request)
    if jwt_token is None:
        return HttpResponseRedirect(reverse('auth:login'))
    header = {'Authorization': 'JWT ' + jwt_token}
    api_response = requests.get(url, headers=header)
    print(api_response)
    if api_response.status_code == 401:
        return HttpResponseRedirect(reverse('auth:login'))
        # return HttpResponse("Error 403: Perboden nih. Mohon login dahulu.")
    api_response = api_response.json()
    response['vacancy'] = api_response
    return render(request, 'vacancy_detail.html', response)
